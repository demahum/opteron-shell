/* LIBRARY SECTION */
#include <ctype.h>              /* Character types                       */
#include <stdio.h>              /* Standard buffered input/output        */
#include <stdlib.h>             /* Standard library functions            */
#include <string.h>             /* String operations                     */
#include <sys/types.h>          /* Data types                            */
#include <sys/wait.h>           /* Declarations for waiting              */
#include <unistd.h>             /* Standard symbolic constants and types */
#include <limits.h>				/* Properties of various variable types  */

/* DEFINE SECTION */
#define SHELL_BUFFER_SIZE 256   /* Size of the Shell input buffer        */
#define SHELL_MAX_ARGS 8        /* Maximum number of arguments parsed    */

/* Definite how many commands we store. */
/* This is not implemented to be modular, but is planned (e.g. to provide value via make -D flag during compilation) */
#ifndef PAST_COMMANDS_COUNT
#define PAST_COMMANDS_COUNT 9
#endif

/* VARIABLE SECTION */
enum { STATE_SPACE, STATE_NON_SPACE };	/* Parser states */

/* Function for running child processes (basically every command except those defined in this file (built-in ones))*/
int imthechild(const char *path_to_exec, char *const args[])
{
	/* Currently suppported paths are /bin/ and current working directory. */
	/* There is room for improvement to make path configurable on the fly. */
	char final_path[80];
	strcpy(final_path, "/bin/");
	if( access( path_to_exec, F_OK ) != -1 ) {
	    return execv(path_to_exec, args) ? -1 : 0;
	} else if ( access(strcat(final_path, path_to_exec), F_OK ) != -1) {
		return execv(final_path, args) ? -1 : 0;
	}
	else {
		printf("%s - command not found\n", path_to_exec);
		return execv(path_to_exec, args) ? -1 : 0;
	}
}

/* Function for handling background processes via parent (main) process.*/
void imtheparent(pid_t child_pid, int run_in_background)
{
	int child_return_val, child_error_code;

	/* fork returned a positive pid so we are the parent */
	if (run_in_background) {
		//don't wait for the child
		return;
	}
	wait(&child_return_val);
	/* Use the WEXITSTATUS to extract the status code from the return value */
	child_error_code = WEXITSTATUS(child_return_val);

	if (child_error_code != 0) {
		/* Error: Child process failed. Most likely a failed exec */
		fprintf(stderr,
		        "  Parent says 'Child process %d failed with code %d'\n",
		        child_pid, child_error_code);
	}
}

/* MAIN PROCEDURE SECTION */
int main(int argc, char **argv)
{
	pid_t shell_pid, pid_from_fork;
	int n_read, i, parser_state, run_in_background;
	/* set count of arguments to zero speficicially because of how we store past commands (for the first command)*/
	int exec_argc = 0;
	/* buffer: The Shell's input buffer. */
	char buffer[SHELL_BUFFER_SIZE];
	/* temp_buffer: Used for storing past commands. */
	char temp_buffer[SHELL_BUFFER_SIZE];
	/* exec_argv: Arguments passed to exec call including NULL terminator. */
	char *exec_argv[SHELL_MAX_ARGS + 1];

	/* Allow the Shell prompt to display the pid of this process */
	shell_pid = getpid();

	/* Declare pointer to an array for storing past commands together with helper variables.*/
    char *past_commands[PAST_COMMANDS_COUNT] = {};
    char **past_commands_pointer = past_commands;
	int next_to_store = 0;
	int command_count = 0;
	/*Flag for not storing failed commands with !notation*/
	int illegal_past_command = 0;
	/*Maximum size of current working directory for prompt*/
	char cwd[200];

	while (1) {
	/* The Shell runs in an infinite loop, processing input. */
		/* Populate prompt with current working directory information*/
		if (getcwd(cwd, sizeof(cwd)) != NULL) {
			printf("%s> ", cwd);
		} else {
			fprintf(stdout, "DirectoryNotRead> ");
			fflush(stdout);
		}

		/* Store the past command if there was command*/
		if (exec_argc) {
			if (!illegal_past_command){
				past_commands_pointer[next_to_store] = strdup(temp_buffer);
				command_count++;
				if (next_to_store == PAST_COMMANDS_COUNT-1) {
					next_to_store = 0;
				} else {
					next_to_store++;
				}
			} else{
				illegal_past_command = 0;
			}
		}

		/* Read a line of input. */
		if (fgets(buffer, SHELL_BUFFER_SIZE, stdin) == NULL)
			return EXIT_SUCCESS;

		/*Support running past commands*/
		if (buffer[0] == '!'){
			if (buffer[1] == '-'){
				illegal_past_command = 1;
				continue;
			} else {
				if (buffer[2] == '\n'){
					int index = buffer[1] - '0';
					if (index > command_count) {
						printf ("You only executed %d command(s) so far.\n", command_count);
						illegal_past_command = 1;
						continue;
					} else{
						if (command_count - index+1 > PAST_COMMANDS_COUNT){
							printf ("Only %d last commands are available.\n", PAST_COMMANDS_COUNT);
							illegal_past_command = 1;
							continue;
						} else {
							index--;
							while (index > PAST_COMMANDS_COUNT-1){
								index -= PAST_COMMANDS_COUNT;
							}
							strcpy(buffer, past_commands_pointer[index]);
							printf ("%s", buffer);
						}
					}
				} else {
					printf ("It's not possible to use ! with two digit numbers. E.g. !9 will work, but !10 (or higher) will not.\n");
					illegal_past_command = 1;
					continue;
				}
			}
		}
		strcpy(temp_buffer, buffer);
		n_read = strlen(buffer);
		run_in_background = n_read > 2 && buffer[n_read - 2] == '&';
		buffer[n_read - run_in_background - 1] = '\n';

		/* Parse the arguments: the first argument is the file or command *
		 * we want to run.                                                */

		parser_state = STATE_SPACE;
		for (exec_argc = 0, i = 0;
		     (buffer[i] != '\n') && (exec_argc < SHELL_MAX_ARGS); i++) {

			if (!isspace(buffer[i])) {
				if (parser_state == STATE_SPACE)
					exec_argv[exec_argc++] = &buffer[i];
				parser_state = STATE_NON_SPACE;
			} else {
				buffer[i] = '\0';
				parser_state = STATE_SPACE;
			}
		}

		/* run_in_background is 1 if the input line's last character *
		 * is an ampersand (indicating background execution).        */


		buffer[i] = '\0';	/* Terminate input, overwriting the '&' if it exists */

		/* If no command was given (empty line) the Shell just prints the prompt again */
		if (!exec_argc)
			continue;
		/* Terminate the list of exec parameters with NULL */
		exec_argv[exec_argc] = NULL;

		/* If Shell runs 'exit' it exits the program. */
		if (!strcmp(exec_argv[0], "exit")) {
			printf("Exiting process %d\n", shell_pid);
			return EXIT_SUCCESS;	/* End Shell program */

		} else if (!strcmp(exec_argv[0], "cd") && exec_argc > 1) {
		/* Running 'cd' changes the Shell's working directory. */
			if (chdir(exec_argv[1]))
				/* Error: change directory failed */
				fprintf(stderr, "cd: failed to chdir %s\n", exec_argv[1]);

		/* Print history as a list */
		/* Should be fixed as it really only nicely works for the first 9 commands*/
		} else if (!strcmp(exec_argv[0], "history")) {
			int i = 0;
			for( i = 0; i < command_count; i++) {
    			printf ("%d   %s", i+1, past_commands[i]);
			}

		} else {
		/* Execute Commands */
			pid_from_fork = fork();

			if (pid_from_fork < 0) {
				/* Error: fork() failed.  Unlikely, but possible (e.g. OS *
				 * kernel runs out of memory or process descriptors).     */
				fprintf(stderr, "fork failed\n");
				continue;
			}
			if (pid_from_fork == 0) {
				return imthechild(exec_argv[0], &exec_argv[0]);
				/* Exit from main. */
			} else {
				imtheparent(pid_from_fork, run_in_background);
				/* Parent will continue around the loop. */
			}
		} /* end if */
	} /* end while loop */

	return EXIT_SUCCESS;
} /* end main() */
